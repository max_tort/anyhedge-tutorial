// Most of the parameters are stored in .env file. Please take a look at the parameters needed.
let env = require('dotenv').config()
env = require('dotenv-parse-variables')(env.parsed);

// importing AnyHedge library
const { AnyHedgeManager } = require('@generalprotocols/anyhedge')

// getting environment variables from .env
const {
  NOMINAL_UNITS,
  CONTRACT_EARLIEST_LIQUIDATION_MODIFIER,
  CONTRACT_MATURITY_MODIFIER,
  CONTRACT_LOW_LIQUIDATION_PRICE_MULTIPLIER,
  CONTRACT_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
  HEDGE_WIF,
  LONG_WIF,
  AUTHENTICATION_TOKEN,
  ORACLE_PUBLIC_KEY
} = env

// utility functions are declared in utility.js
const {
  getStartConditions,
  parseWIF,
  buildFundingTransaction,
} = require('./utility')

const anyHedgeManager = new AnyHedgeManager(AUTHENTICATION_TOKEN);

(async function () {
  // Collect all the parameters that we need to create a contract
  const [contractStartPrice, contractStartHeight] = await getStartConditions()
  const [hedgePrivateKey, hedgePublicKey, hedgeAddress] = await parseWIF(HEDGE_WIF)
  const [longPrivateKey, longPublicKey, longAddress] = await parseWIF(LONG_WIF)

  // Gather all contract creation parameters.
  const contractCreationParameters = [
    ORACLE_PUBLIC_KEY,
    hedgePublicKey,
    longPublicKey,
    NOMINAL_UNITS,
    contractStartPrice,
    contractStartHeight,
    CONTRACT_EARLIEST_LIQUIDATION_MODIFIER,
    CONTRACT_MATURITY_MODIFIER,
    CONTRACT_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
    CONTRACT_LOW_LIQUIDATION_PRICE_MULTIPLIER,
  ]

  try {
    // Declare contractData.
    let contractData;

    try {
      // Retrieve contract data from the settlement service if a contract with these details is already registered.
      const { address } = await anyHedgeManager.createContract(...contractCreationParameters);
      contractData = await anyHedgeManager.getContractStatus(address);

      // Log the contract address for easierc                                                                                           debugging.
      console.log(`Retrieved registered contract data for '${contractData.address}' from the settlement service.`);
    }
    catch (error) {
      // If no contract is registered under this address yet, we register it with the settlement service.
      contractData = await anyHedgeManager.registerContractForSettlement(...contractCreationParameters);

      // Log the contract address for easier debugging.
      console.log(`Registered '${contractData.address}' for automated settlement after funding is complete.`);
    }

    // Build a transaction that funds the contract and pays a service fee to the settlement service provider.
    const fundingTransaction = await buildFundingTransaction(
      hedgePrivateKey, hedgePublicKey, hedgeAddress, longPrivateKey, longPublicKey, longAddress, contractData,
    );

    // Output the raw hex-encoded funding transaction to the console.
    console.log(`Funding transaction: ${fundingTransaction}`);

    // Send the funding transaction to the settlement service for validation and broadcasting.
    await anyHedgeManager.submitFundingTransaction(contractData.address, fundingTransaction);

    // Log the next steps.
    console.log(`Wait for ${CONTRACT_MATURITY_MODIFIER} block(s) and the redemption service should mature your contract, paying out to hedge (${hedgeAddress}) and long (${longAddress}).`);
  }
  catch (error) {
    // Output the error to the console on a new line.
    console.log('\n', error.toString());
  }
})()